import requests
import json
import csv
from datetime import datetime
from time import mktime, strptime, sleep



BASE_URL = "https://map.muoversinpiemonte.it/otp/routers/mip/plan?fromPlace={}%3A%3A{}%2C{}&toPlace={}%3A%3A{}%2C{}&date={}&time={}%3A{}&mode={}&maxWalkDistance=2000&arriveBy={}&wheelchair=false&locale=it"

COMUNI_ORIGIN = "./data/Comuni.csv"
COMUNI_DESTINATION = "./data/Comuni.csv"

# tolleranza sulla data di arrivo/partenza prevista contro quella pianificata
DEVIAZIONE_DATA_SEC=10800 # 10800s == 3h


MAX_RETRIES=5


def prendiItinerario(from_name, from_lat, from_lon, to_name, to_lat, to_lon, date, time_h, time_m, mode="transit", arrive_by="false", errorfile_basename="output"):
    # sostituisco gli spazi a mano, problema con urllib su MacOS
    from_name=from_name.replace(' ', '%20')
    to_name=to_name.replace(' ', '%20')
    
    mode_url="TRANSIT%2CWALK"
    if mode == "car":
        mode_url="CAR"
    
    url = BASE_URL.format(from_name, from_lat, from_lon, to_name, to_lat, to_lon, date, time_h, time_m, mode_url, arrive_by)
    result_itinerario = {
        "duration": 0,
        "transfers": 0,
        "startTime": 0,
        "endTime": 0,
        "walkDistance": 0
    }
    itinerario = None

    error_filename="%s-error.log" % errorfile_basename
    # apri error_log
    error_log = open(error_filename, "a", buffering=1)
    
    retries=0
    while retries<MAX_RETRIES:
        sleep(retries)
        retries += 1
        try:
        
            #print ("url=%s" % url)
            r = requests.get(url, timeout=10)
            content = r.content
            try:
                result = json.loads(content)
            
                if "plan" in result:
                    if "itineraries" in result["plan"]:
                        for itinerario_cur in result["plan"]["itineraries"]:
                        
                            # controlla che l'itinerario proposto ricada in +/- ore dalla data/ora desiderata
                            timestamp_riferimento=int(itinerario_cur["endTime"])/1000
                            if arrive_by == "false":
                                 timestamp_riferimento=int(itinerario_cur["startTime"])/1000
                        
                            timestamp_desiderato=int(mktime(strptime("%s %s:%s" % (date, time_h, time_m), "%Y-%m-%d %H:%M")))
                        
                            if abs(timestamp_desiderato - timestamp_riferimento) < DEVIAZIONE_DATA_SEC:
                                if itinerario is None or int(itinerario_cur["duration"]) < int(itinerario["duration"]):
                                    itinerario=itinerario_cur
            
            except Exception as e:
                print(e)
                print("la risposta da %s non contiene un JSON valido %s" % (url, content))
                error_log.write("%s\nla risposta da %s non contiene un JSON valido %s\n\n" % (e, url, content))
        
            if itinerario is not None:
                # trovato un itinerario
                result_itinerario["duration"] = itinerario["duration"]
                result_itinerario["transfers"] = itinerario["transfers"]
                result_itinerario["startTime"] = itinerario["startTime"]
                result_itinerario["endTime"] = itinerario["endTime"]
                result_itinerario["walkDistance"] = itinerario["walkDistance"]
            break # non sono necessari retries
        
        except Exception as e:
            if retries >= MAX_RETRIES:
                print(e)
                print("problem connecting to %s" % url)
                error_log.write("%s\nproblem connecting to %s\n\n" % (e, url))
                
    error_log.close()
    
    return result_itinerario



def prendiComuni(origDest="orig", maxResults=9999999, offset=0):
    result=[]
    filename=COMUNI_DESTINATION
    if origDest == "orig":
        filename=COMUNI_ORIGIN
    
    with open(filename) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in csvreader:
            if maxResults > 0:
                if offset == 0:
                    result.append(row)
                    maxResults -= 1
                else:
                    offset -= 1
            
    
    
    return result



def prendiItinerarioDummy(from_name, from_lat, from_lon, to_name, to_lat, to_lon, date, time_h, time_m, mode="transit", arrive_by="false"):
    return {
        "duration": 0,
        "transfers": 0,
        "startTime": 0,
        "endTime": 0,
        "walkDistance": 0
    }

