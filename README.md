# ires-trasporti

Tool per estrarre informazioni sui tempi di percorrenza dalle API di 5T Torino (`map.muoversinpiemonte.it/otp/routers/mip/plan`)

## installazione
### aggiornamento di pip (solo per MacOS)
```
curl https://bootstrap.pypa.io/get-pip.py | sudo python
```

### installazione pacchetti richiesti
```
virtualenv envIres
source ./envIres/bin/activate
pip install -r requirements.txt
```

## uso
```
$ python calcola.py
usage: calcola.py <data yyyy-mm-dd> <ora> <minuti> <arrivo|partenza> <transit|car> [<origin_max> <origin_offset> <dest_max> <dest_offset>] [filename discriminator (for concurrency)]
$ 
```

data nel formato yyyy-mm-dd

ora, due cifre

minuti, due cifre

"arrivo" o "partenza" per specificare se l'ora deve considerarsi come ora di arrivo o partenza

"transit" o "car" per specificare il modo di trasporto ("transit" si traduce in TRANSIT+WALK nelle opzioni di 5T)

numero di comuni di origine da prendere

offset nella lista dei comuni di origine

numero di comuni di destinazione da prendere

offset nella lista dei comuni di destinazione

stringa per differenziare i file di output quando si lancia il tool in parallelo (per es. "part1")




### output
il tool genera un file CSV nel formato
```
Nome comune di origine, lat, lon, Nome del comune di destinazione, lat, lon, tempo di percorrenza in secondi, data/ora partenza, data/ora arrivo, numero di cambi
```


