import time
import sys
from datetime import datetime
from itinerari_functions import prendiComuni, prendiItinerario, prendiItinerarioDummy


DATE="2019-11-16"
TIME_H="09"
TIME_M="00"
MODE="transit"

ORIG_MAX=2
ORIG_OFFSET=0
DEST_MAX=2
DEST_OFFSET=0


ARRIVE_BY="true" # in questo caso la data specificata rappresenta l'ora di arrivo desiderata
#ARRIVE_BY="false" # in questo caso la data specificata rappresenta l'ora di partenza desiderata




if len(sys.argv) != 6 and len(sys.argv) != 10 and len(sys.argv) != 11:
    print("usage: %s <data yyyy-mm-dd> <ora> <minuti> <arrivo|partenza> <transit|car> [<origin_max> <origin_offset> <dest_max> <dest_offset>] [filename discriminator (for concurrency)]" % sys.argv[0])
    exit(-1)

DATE=sys.argv[1]
TIME_H=sys.argv[2]
TIME_M=sys.argv[3]
MODE=sys.argv[5]

ARRIVE_BY="true"
if sys.argv[4] == "partenza":
    ARRIVE_BY="false"

if len(sys.argv) == 10 or len(sys.argv) == 11:
    ORIG_MAX=int(sys.argv[6])
    ORIG_OFFSET=int(sys.argv[7])
    DEST_MAX=int(sys.argv[8])
    DEST_OFFSET=int(sys.argv[9])

FILENAME_DISCR=""
if len(sys.argv) == 11:
    FILENAME_DISCR=sys.argv[10]
    


orario="partenza"
if ARRIVE_BY == "true":
    orario="arrivo"





def calcola(orig_max=999999, orig_offset=0, dest_max=999999, dest_offset=0, filename_discriminator=""):
    outfile_csv="./output/out-itinerari_%s_%s_%s-%s_%s%s.csv" % (MODE, DATE, TIME_H, TIME_M, orario, filename_discriminator)

    print("\ncalcolo tempi per:\n    data: %s %s:%s (orario di %s)\n    mezzo: %s" % (DATE, TIME_H, TIME_M, orario, MODE))
    print("    limite origini: %s\n    inizia dall'origine: %s" % (orig_max, orig_offset))
    print("    limite destinazioni: %s\n    inizia dalla destinazione: %s" % (dest_max, dest_offset))
    print("    output file: %s\n\n" % outfile_csv)
    
    # controlla se il file esiste
    try:
        f = open(outfile_csv)
        f.close()
        
        print ("\n****** Errore")
        print ("il file %s esiste!!" % outfile_csv)
        print ("salvarlo e cancellarlo per proseguire...\n\n")
        exit(-1)
    except Exception:
        pass
    
    
    csv_out = open(outfile_csv, "a", buffering=1)
    csv_out.truncate(0) # questo svuota il file
    
    # cancella error log
    error_filename="%s-error.log" % outfile_csv
    error_log = open(error_filename, "a")
    error_log.truncate(0) # questo svuota il file
    error_log.close()
    
    start = time.time()
    count_results=0

    #print(prendiItinerario("Torino", "45.07136", "7.66641", "Cuneo", "44.38956", "7.54787", "2019-11-16", "15", "00"))

    comuni_or=prendiComuni("orig", orig_max, orig_offset)
    orig_count=len(comuni_or)
    comuni_dest=prendiComuni("dest", dest_max, dest_offset)
    dest_count=len(comuni_dest)

    orig_cur=0
    for comune_or in comuni_or:
        orig_name=comune_or[0]
        orig_lon=comune_or[1]
        orig_lat=comune_or[2]
        #print("%-30s (%s,%s)" % (orig_name, orig_lon, orig_lat))
        dest_cur=0
        for comune_dest in comuni_dest:

            dest_name=comune_dest[0]
            dest_lon=comune_dest[1]
            dest_lat=comune_dest[2]
            dest_cur += 1
            if orig_name != dest_name:
                itinerario=prendiItinerario(orig_name, orig_lat, orig_lon, dest_name, dest_lat, dest_lon, DATE, TIME_H, TIME_M, MODE, ARRIVE_BY, outfile_csv)
                
                arrivo="--/--/---- --:--"
                endTime=int(itinerario["endTime"])/1000
                if endTime > 0:
                    arrivo=datetime.fromtimestamp(endTime)
                    arrivo=arrivo.strftime("%d/%m/%Y %H:%M")
                partenza="--/--/---- --:--"
                startTime=int(itinerario["startTime"])/1000
                if endTime > 0:
                    partenza=datetime.fromtimestamp(startTime)
                    partenza=partenza.strftime("%d/%m/%Y %H:%M")
                
                indicazione_comuni="%s - %s" % (orig_name, dest_name)
                
                progress = float(dest_cur + orig_cur * dest_count) * 100 / (orig_count * dest_count)
                
                print ("[%3dmin %5.1f%%] %-48s %8s sec (part. %s, arr. %s, %s cambi)" % ((time.time()-start)/60, progress, indicazione_comuni, itinerario["duration"], partenza, arrivo, itinerario["transfers"]))
                csv_out.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (orig_name, orig_lat, orig_lon, dest_name, dest_lat, dest_lon, itinerario["duration"], partenza, arrivo, itinerario["transfers"]))
                
                count_results += 1
                

                
                
        orig_cur += 1

    end = time.time()
    print("%s results, elapsed time %.2f s" % (count_results, end - start))




calcola(ORIG_MAX,ORIG_OFFSET,DEST_MAX,DEST_OFFSET, FILENAME_DISCR)
#calcola()



